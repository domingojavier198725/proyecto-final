from sqlalchemy import create_engine,MetaData,Column, String,Table
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy_utils import database_exists, create_database
from sqlalchemy_serializer import SerializerMixin
import random

Base = declarative_base() 

class CitaModel (Base,SerializerMixin):
    __tablename__= "Citas"
    id_ = Column(String,primary_key=True)
    name = Column(String)
    phone = Column(String)
    mail = Column(String)
    message = Column(String)
    time = Column(String)
    def __init__(self,id_,name,phone,mail,message,time):
        self.id_ = id_
        self.name = name
        self.phone = phone
        self.mail = mail
        self.message = message
        self.time = time

def createTable(engine): 
    meta = MetaData()
    mydb = Table(
    'Citas',meta,
    Column('id_',String(1000),primary_key=True),
    Column('name',String(1000)),
    Column('phone',String(1000)),
    Column('mail',String(1000)),
    Column('message',String(1000)),
    Column('time',String(1000)))
    meta.create_all(engine)


class ORM():
    def __init__(self,nameTable,dialect): 
        self.nameTable = nameTable+'.db' 
        self.dialect = dialect+self.nameTable +'?check_same_thread=False' 
        self.engine = create_engine(self.dialect)  
        if not database_exists(self.engine.url): 
            create_database(self.engine.url)
            createTable(self.engine)
        self.session = sessionmaker(bind = self.engine)() 

    def agregarCita(self,name,phone,mail,message,time):
        self.session.add(CitaModel(str(self.session.query(CitaModel).count() + 1),name,phone,mail,message,time))
        self.session.commit()
    
    def obtenerCita(self,id_):
        try:
            resultado = self.session.query(CitaModel).filter(CitaModel.id_ == id_).one()
            return resultado
        except:
            pass
       
    
    def obtenerTodo(self):
        resultado = self.session.query(CitaModel).all()
        return resultado
    
    def editarCita(self,id_,time,message):
        resultado = self.session.query(CitaModel).filter(CitaModel.id_ == id_).one()
        resultado.message = message
        resultado.time = time
        self.session.add(resultado)
        self.session.commit()
    
    def borrarCita(self,id_):
        resultado = self.session.query(CitaModel).filter(CitaModel.id_ == id_).one()
        self.session.delete(resultado)
        self.session.commit()

#dialectoSQLITE = 'sqlite:///'
#sql = ORM('MyTableName',dialectoSQLITE)
#DIALECTO PARA MARIADB
#sql = ORM('MyTableName',dialectoMARIADB)
#Estos son algunos ejemplo de lo que puedes hacer
#sql.agregarCita('newPalabra','newDefinicion','asdf','asdf','sdf')
#sql.editarPalbra('palabra','palabraEditada')
#sql.editarDefinicion('palabra','definicionEditada')
#sql.obtenerTodo()
#sql.borrarPalabra('palabra')
