from flask import Flask,jsonify,render_template,request,redirect
from flask_restful import Api, Resource, reqparse
from marshmallow import Schema, fields
from Orm import ORM

class CitaModel(Schema):
    name = fields.Str()
    phone = fields.Str()
    mail = fields.Str()
    message = fields.Str()
    time = fields.Str()

app = Flask(__name__)
api = Api(app)

Palabras = [{'id':1,'title':'mtitle'}]

class Citas(Resource):
     dialectoSQLITE = 'sqlite:///'
     orm = ORM('Citas',dialectoSQLITE)
     def get(self, id=''): 
         if id == '':
            return CitaModel(many=True).dump(self.orm.obtenerTodo()), 200
         
         if CitaModel().dump(self.orm.obtenerCita(id)):
             return CitaModel().dump(self.orm.obtenerCita(id))
         else:
             return "No hay resultado", 404

     def post(self):
        name = request.form['name']
        phone = request.form['phone']
        mail = request.form['mail']
        time = request.form['time']
        message = request.form['message']
        id = request.form['id']
        type = request.form['type']
        try:
            if(type == "post"):
                self.orm.agregarCita(name,phone,mail,message,time)
                return redirect('/')
            else:
                self.orm.editarCita(id,time,message)
                return redirect('/')

        except:
            return f"error la cita no fue creada", 400
        

     def put(self):
        id = request.form['id']
        time = request.form['time']
        message = request.form['message']        
        try:
            return 'cambio exitoso', 200
        except:
            return f"la cita {params['id']} no  existe", 400

     def delete(self):
        parser = reqparse.RequestParser()
        parser.add_argument("id")
        params = parser.parse_args()
        try:
            self.orm.borrarCita(params['id'])
            return f"la cita {params['id']} ha sido borrado", 200 
        except:
            return f"la cita {params['id']} no existe", 400
      

api.add_resource(Citas, "/citas", "/citas/", "/citas/<string:id>")

@app.route('/',methods=['GET','POST'])
def index():
    dialectoSQLITE = 'sqlite:///'
    orm = ORM('Citas',dialectoSQLITE) 
    tabla = orm.obtenerTodo()
    if 'refrescar' in request.form:
        try:
            orm = ORM('Citas',dialectoSQLITE)
            tabla = orm.obtenerTodo()
        except:
            pass
    return render_template('index.html',tabla=tabla)

if __name__ == '__main__':
    app.run(debug=True)
